package reports_html;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;
import com.aventstack.extentreports.reporter.configuration.Theme;

public class reports_html {

	public WebDriver driver;
	
	public ExtentHtmlReporter htmlReporter;
	public ExtentReports extent;
	public ExtentTest test;
	
	
	@BeforeTest
	public void ExtentReports()
	{
		htmlReporter=new ExtentHtmlReporter(System.getProperty("user.dir")+"/test-output/ExtentReports.html");
		
		htmlReporter.config().setDocumentTitle("MY TEST REPORT");
		htmlReporter.config().setReportName("GOOGLE TESTY TREPORT");
		htmlReporter.config().setTheme(Theme.STANDARD);
	    
		extent=new ExtentReports();
	    extent.attachReporter(htmlReporter);
	
	    extent.setSystemInfo("HOST NAME", "LOCAL HOST");
	    extent.setSystemInfo("TESTER NAME", "JUVERIA");
	}
	
	@AfterTest
	public void endReport()
	{
		extent.flush();
	}
	
	@BeforeMethod
	public void setup()
	{
		System.setProperty("webdriver.chrome.driver",System.getProperty("user.dir")+"\\drivers\\chromedriver.exe");
		driver=new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("https://www.amazon.ca/");
		
	}
	
@Test
public void amazontest()
{
	test=extent.createTest("amazontest");
	String title=driver.getTitle();
	System.out.println(title);
	driver.findElement(By.xpath("//input[@type='text']")).sendKeys("bead jewellry");
	driver.findElement(By.xpath("//input[@type='submit']")).click();
	
	
}
@AfterMethod
public void tearDown(ITestResult result)
{
	if(result.getStatus()==ITestResult.FAILURE)
	{
		test.log(Status.FAIL, "The Test case is failed"+result.getName());
		
	}
	else if(result.getStatus()==ITestResult.SUCCESS)
	{
		test.log(Status.PASS, "The test case is passed"+result.getName());
	}
}
	
	
}
